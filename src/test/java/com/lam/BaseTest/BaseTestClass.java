package com.lam.BaseTest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.lam.util.Log;
import com.lam.util.PropertyFiles;

public class BaseTestClass {

	protected WebDriver driver;
	protected PropertyFiles property;
	private final String  PROPERTY_FILE = "lam.properties";
	private static String usedBrowser = null;
	private static String usedEnvironment = null;

	public BaseTestClass() {
		Log.dynamicApp(System.getProperty("user.dir")+"\\src\\test\\resources\\Logs\\","VCC");
		BasicConfigurator.configure();
	}
	@BeforeMethod
	@Parameters({"browser","environment"})
	public void setUpEnvironment(@Optional String browser,@Optional String environment) throws IOException{
		// In case values are not provided by system env
		if(StringUtils.isEmpty(environment) && StringUtils.isEmpty(browser)){
			browser = "firefox";		
			environment = "https://vcclamresearch.com/SitePages/MyVCC.aspx";
		}
		usedEnvironment = environment;
		if (browser.equalsIgnoreCase("firefox")){
			usedBrowser = "firefox";			
			driver = new FirefoxDriver();
			driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
		}		
		if (browser.equalsIgnoreCase("ie")){
			usedBrowser = "ie";
			System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
			driver = new InternetExplorerDriver(capabilities);
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		}
		if (browser.equalsIgnoreCase("chrome")){
			usedBrowser = "chrome";
			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		}	
		driver.manage().window().maximize();		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		property = new PropertyFiles();
		property.loadPropertyFile(PROPERTY_FILE);
	}

	/**
	 * This method is called after logout and login again
	 * @throws IOException
	 */
	public void setUpEnvironment() throws IOException{
		if (usedBrowser.equalsIgnoreCase("firefox")){
			driver = new FirefoxDriver();
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		}	
		if (usedBrowser.equalsIgnoreCase("ie")){			
			System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		}
		if (usedBrowser.equalsIgnoreCase("chrome")){
			usedBrowser = "chrome";
			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		}
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		property = new PropertyFiles();
		property.loadPropertyFile(PROPERTY_FILE);
	}

	public static String getBrowser(){		
		return usedBrowser;
	}

	public static String getEnvironment(){
		return usedEnvironment;
	}
}
