package com.lam.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFiles {
	private Properties property ;
	final String PROPERTY_FILE_LOCATION = "src/test/resources/data/propertyFiles/";
	
	public void loadPropertyFile(String fileName){
		Log.info(PROPERTY_FILE_LOCATION+fileName);
		FileInputStream fis = null;
		property = new Properties();
		try {
			fis = new FileInputStream(PROPERTY_FILE_LOCATION+fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.info("File Not Found");
		}
		try {
			property.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.info("File Not loaded");

		}
	}

	public String getProperty(String key){
		Log.info(property.getProperty(key));
		return property.getProperty(key);
	}
	
}
