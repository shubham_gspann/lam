package com.lam.tests.SanityTests;

import java.awt.AWTException;
import java.io.IOException;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertEquals;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.lam.BaseTest.BaseTestClass;
import com.lam.pages.Header;
import com.lam.pages.HomePage;
import com.lam.pages.LeftnavigationPanel;
import com.lam.pages.LoginPage;
import com.lam.pages.VCCCollaborationPage;
import com.lam.pages.VCCExecutivePage;
import com.lam.pages.VCCManagementPage;
import com.lam.util.Log;

public class SanityTestClass extends BaseTestClass{
	private String URL = null;
	private String lamPMUsername = null;
	private String cstUserUsername = null;
	private String password = null;
	private String fileToUpload = null;

	private LoginPage login;
	private HomePage home;
	private Header header;
	private VCCCollaborationPage vccCollaboration;
	private VCCManagementPage vccManagement;
	private VCCExecutivePage vccExecutive;
	private LeftnavigationPanel leftPanel;

	@BeforeMethod
	public void loginToApplication() throws InterruptedException, IOException{
		login = new LoginPage(driver);
		URL = getEnvironment();
		lamPMUsername = property.getProperty("lam_PM_username");
		password = property.getProperty("password");
		home = login.loginToApplication(URL,lamPMUsername,password);
		assertTrue("Home Page is not loaded",home.verifyVCCCollaborationTab());
	}


	
	@Test(enabled=true)
	public void testVerifyVCCTabs() throws InterruptedException{
		assertTrue("VCC Collaboration Tab is not present on home page",home.verifyVCCCollaborationTab());
		assertTrue("VCC Management Tab is not present on home page",home.verifyVCCManagementTab());
		assertTrue("VCC Executive Tab is not present on home page",home.verifyVCCExecutiveTab());
		assertTrue("VCC Mail Tab is not present on home page",home.verifyVCCMailTab());
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws AWTException 
	 */
	@Test(enabled=true,priority=1)
	public void testSuccessfulUploadFile() throws InterruptedException, IOException, AWTException{
		Log.info("---------------Test successfulUploadFile started-----------------");
		vccCollaboration=home.clickOnVCCCollaborationTab();
		fileToUpload = property.getProperty("file_to_upload");
		assertTrue("File has not uploaded successfully",vccCollaboration.uploadNewDocumentAndVerifyInRows(fileToUpload));
		vccCollaboration.clickOnOpenMenuDots(fileToUpload);
		assertTrue("File is not encrypted successfully",vccCollaboration.isFileInEncryptedForm(fileToUpload));
		vccCollaboration.clickOnOpenSubMenuDots(fileToUpload);	
		vccCollaboration.selectAndLoginToViewInSecureCollaboration(lamPMUsername, password);
		vccCollaboration.switchToLastOpenedWindow();
		assertFalse("Print button is not displayed on VCC Secure Collaboration",vccCollaboration.isPrintButtonEnabled());
		assertTrue("Right Click is not disabled on VCC Secure Collaboration",vccCollaboration.isRightClickDisabled());
		Log.info("---------------Test successfulUploadFile ended-----------------");
	}

	/**
	 *  
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws AWTException 
	 */
	@Test(enabled=true,priority=2)
	public void testSuccessfulManageAccess() throws InterruptedException, IOException, AWTException{
		Log.info("---------------Test SuccessfulManageAccess started-----------------");

		header =  new Header(driver);
		vccCollaboration=home.clickOnVCCCollaborationTab();
		cstUserUsername = property.getProperty("cst_User_username");		
		fileToUpload = property.getProperty("file_to_upload");
		assertTrue("File has not uploaded successfully",vccCollaboration.uploadNewDocumentAndVerifyInRows(fileToUpload));
		vccCollaboration.clickOnOpenMenuDots(fileToUpload);
		vccCollaboration.clickOnOpenSubMenuDots(fileToUpload);	
		vccCollaboration.selectFromMenuItems("Manage Access");		
		vccCollaboration.manageAccessForUser(cstUserUsername);
		header.clickSignOutCompletely();		
		setUpEnvironment();
		login = new LoginPage(driver);
		URL = getEnvironment();
		password = property.getProperty("password");
		home = login.loginToApplication(URL,cstUserUsername,password);
		vccCollaboration=home.clickOnVCCCollaborationTab();
		vccCollaboration.clickOnSharedDocumentIcon();
		assertTrue("File is not visible in documents for shared user",vccCollaboration.waitAndverifyForFileVisibleInRows(fileToUpload));
		vccCollaboration.clickOnOpenMenuDots(fileToUpload);
		assertTrue("File is not encrypted successfully",vccCollaboration.isFileInEncryptedForm(fileToUpload));
		vccCollaboration.clickOnOpenSubMenuDots(fileToUpload);	
		vccCollaboration.selectAndLoginToViewInSecureCollaboration(cstUserUsername, password);		
		vccCollaboration.switchToLastOpenedWindow();
		assertFalse("Print button is not displayed on VCC Secure Collaboration",vccCollaboration.isPrintButtonEnabled());
		assertTrue("Right Click is not disabled on VCC Secure Collaboration",vccCollaboration.isRightClickDisabled());
		Log.info("---------------Test SuccessfulManageAccess ended-----------------");
	}

	/**
	 * 	  
	 * 
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	@Test(enabled=true,priority=3)
	public void testSuccessfulDeletionOfFile() throws InterruptedException, AWTException, IOException{
		Log.info("---------------Test SuccessfulDeletionOfFile started-----------------");
		fileToUpload = property.getProperty("file_to_upload");		
		leftPanel = new LeftnavigationPanel(driver);
		vccCollaboration=home.clickOnVCCCollaborationTab();
		assertTrue("File has not uploaded successfully",vccCollaboration.uploadNewDocumentAndVerifyInRows(fileToUpload));
		vccCollaboration.clickOnOpenMenuDots(fileToUpload);
		vccCollaboration.clickOnOpenSubMenuDots(fileToUpload);	
		vccCollaboration.selectFromMenuItems("Delete Item");
		assertEquals("Delete confirmation message is not present","Are you sure you want to delete "+fileToUpload,vccCollaboration.getDeleteConfirmationMsg());
		vccCollaboration.selectConfirmAndDeleteSelectedFile(fileToUpload);				
		assertEquals("Deletion successful message is not present","Document "+fileToUpload+" has been successfully deleted.", vccCollaboration.getDeleteSuccessMsgAppears());
		vccCollaboration.clickOnCloseAfterDeleteMsgPopup(fileToUpload);
		assertFalse("File is present in rows even after deletion",vccCollaboration.isTheFilePresentInRows(fileToUpload));
		home = leftPanel.clickBackToVCC();
		vccManagement=home.clickOnVCCManagementTab();
		vccManagement.clickOnCertificationOfDestruction();
		assertTrue("Deleted file is not present at first row in Certification Of Destruction",vccManagement.isTheFilePresentAtFirstRowOfCertificationDestruction(fileToUpload));
		assertTrue("File doesn't contains 'docx' extension",vccManagement.isFileContainsDocxExtension(fileToUpload));
		home = leftPanel.clickBackToVCC();
		home.clickOnVCCManagementTab();
		vccManagement.clickOnDeletionTracker();
		assertTrue("Deleted file is not present at first row in Deletion Trackor",vccManagement.isTheFilePresentAtFirstRowOfDeletionTracker(fileToUpload));
		Log.info("---------------Test SuccessfulDeletionOfFile ended-----------------");
	}

	/**
	 * 
	 * 
	 * 
	 * @throws InterruptedException
	 */
	@Test(enabled=true,priority=4)
	public void testVerifyBIDashBoard() throws InterruptedException{		
		Log.info("---------------Test VerifyBIDashBoard started-----------------");
		lamPMUsername = property.getProperty("lam_PM_username");
		password = property.getProperty("password");
		vccExecutive=home.clickOnVCCExecutiveTab(lamPMUsername,password); 
		Log.info("User validation successful for logging into Vcc Executive portal");
		assertTrue("Executive Dashboard-Micron display has error",vccExecutive.validateExecDashBoardMicron("Executive Dashboard"));
		vccExecutive.clickBITab("Hours by Tier");
		assertTrue("Micron Install Dashboard display has error",vccExecutive.validateMicronInstallDasboard("Install Hours by Tier and Equipment Dashboard"));
		vccExecutive.clickBITab("Supplier Scorecard");
		assertTrue("Micron Supplier ScoreCard display has error",vccExecutive.validateMicronSupplierScorecardDashboard("Supplier Scorecard"));
		Log.info("---------------Test VerifyBIDashBoard ended-----------------");
	}


	@AfterMethod
	public void tearDown(){
		driver.quit();
	}

}

