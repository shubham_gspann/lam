package com.lam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.internal.Locatable;

import com.lam.BasePage.BasePageClass;
import com.lam.util.Log;


public class VCCExecutivePage extends BasePageClass{

	private final By PAGE_VIEWER_IFRAME_LOC = By.xpath("//iframe[@title='Page Viewer']");
	private final By PAGE_TITLE_LOC = By.xpath("//h3[@class='page-title']");
	public VCCExecutivePage(WebDriver driver) {
		super(driver);		
	}

	public VCCExecutivePage clickBITab(String tabName) throws InterruptedException{
		driver.findElement(By.xpath("//span[@class='title' and text()='"+tabName+"']")).click();	
		Thread.sleep(5000);
		return new VCCExecutivePage(driver);
	}
	
	public boolean validateExecDashBoardMicron(String title) throws InterruptedException{
		waitForElementVisible(PAGE_VIEWER_IFRAME_LOC);		  
		((Locatable)driver.findElement(PAGE_TITLE_LOC)).getCoordinates().inViewPort();
		if(driver.getCurrentUrl().contains("ExecDashboard2")
				&& driver.findElement(PAGE_VIEWER_IFRAME_LOC).isDisplayed()
				&& driver.findElement(PAGE_TITLE_LOC).getText().equals(title)){
			driver.switchTo().frame(driver.findElement(PAGE_VIEWER_IFRAME_LOC));
			try{
				driver.findElement(By.xpath("//header[@class='dm-header ng-scope' and text()='An error occurred']"));
				Log.info("An error Occured in Exec Dash Board Micron");
				driver.switchTo().defaultContent();
				return false;
			}catch(NoSuchElementException e){
				driver.switchTo().defaultContent();
				return true;
			}			
		}
		return false;
	}


	public boolean validateMicronInstallDasboard(String title) throws InterruptedException{
		waitForElementVisible(PAGE_VIEWER_IFRAME_LOC);
		((Locatable)driver.findElement(PAGE_TITLE_LOC)).getCoordinates().inViewPort();
		if(driver.getCurrentUrl().contains("ExecDashboard3")
				&& driver.findElement(PAGE_VIEWER_IFRAME_LOC).isDisplayed()
				&& driver.findElement(PAGE_TITLE_LOC).getText().equals(title)){
			driver.switchTo().frame(driver.findElement(PAGE_VIEWER_IFRAME_LOC));
			try{
				driver.findElement(By.xpath("//header[@class='dm-header ng-scope' and text()='An error occurred']"));
				Log.info("An error occured in Micron Install Dasboard");
				driver.switchTo().defaultContent();
				return false;
			}catch(NoSuchElementException e){
				driver.switchTo().defaultContent();
				return true;
			}			
		}
		return false;
	}
	
	public boolean validateMicronSupplierScorecardDashboard(String title) throws InterruptedException{
		waitForElementVisible(PAGE_VIEWER_IFRAME_LOC);		  
		((Locatable)driver.findElement(PAGE_TITLE_LOC)).getCoordinates().inViewPort();
		if(driver.getCurrentUrl().contains("ExecDashboard4")
				&& driver.findElement(PAGE_VIEWER_IFRAME_LOC).isDisplayed()
				&& driver.findElement(PAGE_TITLE_LOC).getText().equals(title)){
			driver.switchTo().frame(driver.findElement(PAGE_VIEWER_IFRAME_LOC));
			try{
				driver.findElement(By.xpath("//header[@class='dm-header ng-scope' and text()='An error occurred']"));
				Log.info("An error Occured in Micron Supplier Scorecard Dashboard");
				driver.switchTo().defaultContent();
				return false;
			}catch(NoSuchElementException e){
				driver.switchTo().defaultContent();
				return true;
			}			
		}
		return false;
	}

}
