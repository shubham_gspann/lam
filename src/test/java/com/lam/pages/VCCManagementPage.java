package com.lam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.lam.BasePage.BasePageClass;


public class VCCManagementPage extends BasePageClass {
	final By NEW_DOCUMENT_LOC=By.xpath("//img[contains(@id,'idHomePageNewDocument')]");
	final By CERT_OF_DESTRUCTI0N_LOC = By.xpath("//a[@title='Certificates of Destruction']");
	final By DELETION_TRACKER_LOC = By.xpath("//a[text()='Item Deletion Tracker']");
	
	String certificationOfDeletionFirstRowLoc = "//script[@id='scriptBodyWPQ2']/following::tr[1]//a[@class='ms-listlink ms-draggable' and contains(text(),'%s.nxl')]"; 
	String deletionTrackerFirstRowLoc = "//script[@id='scriptBodyWPQ2']/following::tr[1]//a[@class='ms-listlink' and contains(text(),'%s')]";

	public VCCManagementPage(WebDriver driver) {
		super(driver);
		}
	
	
	public VCCManagementPage clickOnCertificationOfDestruction() {
		driver.findElement(CERT_OF_DESTRUCTI0N_LOC).click();		
		return new VCCManagementPage(driver);
	}
	
	public VCCManagementPage clickOnDeletionTracker() {
		driver.findElement(DELETION_TRACKER_LOC).click();		
		return new VCCManagementPage(driver);
	}
	
	public boolean isTheFilePresentAtFirstRowOfCertificationDestruction(String fileName){
		try{			
			driver.findElement(By.xpath(String.format(certificationOfDeletionFirstRowLoc, fileName)));
			return true;
		}catch(NoSuchElementException e){
			return false;
		}
	}	
	
	public boolean isFileContainsDocxExtension(String fileName){
		boolean isFileContainsDocxExtension = false;
		String hrefForDocxValidation = driver.findElement(By.xpath(String.format(certificationOfDeletionFirstRowLoc, fileName))).getAttribute("href");
		if(hrefForDocxValidation.endsWith("docx")){
			isFileContainsDocxExtension = true;
			}
		return isFileContainsDocxExtension;
	}
	
	public boolean isTheFilePresentAtFirstRowOfDeletionTracker(String fileName){
		try{
			waitForElementVisible(By.xpath(String.format(deletionTrackerFirstRowLoc,fileName)));
			driver.findElement(By.xpath(String.format(deletionTrackerFirstRowLoc, fileName)));
			return true;
		}catch(NoSuchElementException e){
			return false;
		}
	}
	


}
